/* tslint:disable */
/* eslint-disable */
//  This file was automatically generated and should not be edited.

export type CreateHelpWantedInput = {
  id?: string | null,
  owner: string,
  _version?: number | null,
};

export type ModelHelpWantedConditionInput = {
  and?: Array< ModelHelpWantedConditionInput | null > | null,
  or?: Array< ModelHelpWantedConditionInput | null > | null,
  not?: ModelHelpWantedConditionInput | null,
};

export type UpdateHelpWantedInput = {
  id: string,
  owner?: string | null,
  _version?: number | null,
};

export type DeleteHelpWantedInput = {
  id?: string | null,
  _version?: number | null,
};

export type CreateJobPostInput = {
  id?: string | null,
  owner: string,
  title: string,
  description: string,
  image: string,
  helpWantedID: string,
  _version?: number | null,
};

export type ModelJobPostConditionInput = {
  owner?: ModelStringInput | null,
  title?: ModelStringInput | null,
  description?: ModelStringInput | null,
  image?: ModelStringInput | null,
  helpWantedID?: ModelIDInput | null,
  and?: Array< ModelJobPostConditionInput | null > | null,
  or?: Array< ModelJobPostConditionInput | null > | null,
  not?: ModelJobPostConditionInput | null,
};

export type ModelStringInput = {
  ne?: string | null,
  eq?: string | null,
  le?: string | null,
  lt?: string | null,
  ge?: string | null,
  gt?: string | null,
  contains?: string | null,
  notContains?: string | null,
  between?: Array< string | null > | null,
  beginsWith?: string | null,
  attributeExists?: boolean | null,
  attributeType?: ModelAttributeTypes | null,
  size?: ModelSizeInput | null,
};

export enum ModelAttributeTypes {
  binary = "binary",
  binarySet = "binarySet",
  bool = "bool",
  list = "list",
  map = "map",
  number = "number",
  numberSet = "numberSet",
  string = "string",
  stringSet = "stringSet",
  _null = "_null",
}


export type ModelSizeInput = {
  ne?: number | null,
  eq?: number | null,
  le?: number | null,
  lt?: number | null,
  ge?: number | null,
  gt?: number | null,
  between?: Array< number | null > | null,
};

export type ModelIDInput = {
  ne?: string | null,
  eq?: string | null,
  le?: string | null,
  lt?: string | null,
  ge?: string | null,
  gt?: string | null,
  contains?: string | null,
  notContains?: string | null,
  between?: Array< string | null > | null,
  beginsWith?: string | null,
  attributeExists?: boolean | null,
  attributeType?: ModelAttributeTypes | null,
  size?: ModelSizeInput | null,
};

export type UpdateJobPostInput = {
  id: string,
  owner?: string | null,
  title?: string | null,
  description?: string | null,
  image?: string | null,
  helpWantedID?: string | null,
  _version?: number | null,
};

export type DeleteJobPostInput = {
  id?: string | null,
  _version?: number | null,
};

export type CreateBidInput = {
  id?: string | null,
  owner: string,
  jobPostID: string,
  content: string,
  dollarValue: number,
  _version?: number | null,
};

export type ModelBidConditionInput = {
  owner?: ModelStringInput | null,
  jobPostID?: ModelIDInput | null,
  content?: ModelStringInput | null,
  dollarValue?: ModelFloatInput | null,
  and?: Array< ModelBidConditionInput | null > | null,
  or?: Array< ModelBidConditionInput | null > | null,
  not?: ModelBidConditionInput | null,
};

export type ModelFloatInput = {
  ne?: number | null,
  eq?: number | null,
  le?: number | null,
  lt?: number | null,
  ge?: number | null,
  gt?: number | null,
  between?: Array< number | null > | null,
  attributeExists?: boolean | null,
  attributeType?: ModelAttributeTypes | null,
};

export type UpdateBidInput = {
  id: string,
  owner?: string | null,
  jobPostID?: string | null,
  content?: string | null,
  dollarValue?: number | null,
  _version?: number | null,
};

export type DeleteBidInput = {
  id?: string | null,
  _version?: number | null,
};

export type ModelHelpWantedFilterInput = {
  id?: ModelIDInput | null,
  owner?: ModelStringInput | null,
  and?: Array< ModelHelpWantedFilterInput | null > | null,
  or?: Array< ModelHelpWantedFilterInput | null > | null,
  not?: ModelHelpWantedFilterInput | null,
};

export type ModelJobPostFilterInput = {
  id?: ModelIDInput | null,
  owner?: ModelStringInput | null,
  title?: ModelStringInput | null,
  description?: ModelStringInput | null,
  image?: ModelStringInput | null,
  helpWantedID?: ModelIDInput | null,
  and?: Array< ModelJobPostFilterInput | null > | null,
  or?: Array< ModelJobPostFilterInput | null > | null,
  not?: ModelJobPostFilterInput | null,
};

export type ModelBidFilterInput = {
  id?: ModelIDInput | null,
  owner?: ModelStringInput | null,
  jobPostID?: ModelIDInput | null,
  content?: ModelStringInput | null,
  dollarValue?: ModelFloatInput | null,
  and?: Array< ModelBidFilterInput | null > | null,
  or?: Array< ModelBidFilterInput | null > | null,
  not?: ModelBidFilterInput | null,
};

export type CreateHelpWantedMutationVariables = {
  input: CreateHelpWantedInput,
  condition?: ModelHelpWantedConditionInput | null,
};

export type CreateHelpWantedMutation = {
  createHelpWanted:  {
    __typename: "HelpWanted",
    id: string,
    owner: string,
    jobPosts:  {
      __typename: "ModelJobPostConnection",
      items:  Array< {
        __typename: "JobPost",
        id: string,
        owner: string,
        title: string,
        description: string,
        image: string,
        helpWantedID: string,
        _version: number,
        _deleted: boolean | null,
        _lastChangedAt: number,
        createdAt: string,
        updatedAt: string,
      } | null > | null,
      nextToken: string | null,
      startedAt: number | null,
    } | null,
    _version: number,
    _deleted: boolean | null,
    _lastChangedAt: number,
    createdAt: string,
    updatedAt: string,
  } | null,
};

export type UpdateHelpWantedMutationVariables = {
  input: UpdateHelpWantedInput,
  condition?: ModelHelpWantedConditionInput | null,
};

export type UpdateHelpWantedMutation = {
  updateHelpWanted:  {
    __typename: "HelpWanted",
    id: string,
    owner: string,
    jobPosts:  {
      __typename: "ModelJobPostConnection",
      items:  Array< {
        __typename: "JobPost",
        id: string,
        owner: string,
        title: string,
        description: string,
        image: string,
        helpWantedID: string,
        _version: number,
        _deleted: boolean | null,
        _lastChangedAt: number,
        createdAt: string,
        updatedAt: string,
      } | null > | null,
      nextToken: string | null,
      startedAt: number | null,
    } | null,
    _version: number,
    _deleted: boolean | null,
    _lastChangedAt: number,
    createdAt: string,
    updatedAt: string,
  } | null,
};

export type DeleteHelpWantedMutationVariables = {
  input: DeleteHelpWantedInput,
  condition?: ModelHelpWantedConditionInput | null,
};

export type DeleteHelpWantedMutation = {
  deleteHelpWanted:  {
    __typename: "HelpWanted",
    id: string,
    owner: string,
    jobPosts:  {
      __typename: "ModelJobPostConnection",
      items:  Array< {
        __typename: "JobPost",
        id: string,
        owner: string,
        title: string,
        description: string,
        image: string,
        helpWantedID: string,
        _version: number,
        _deleted: boolean | null,
        _lastChangedAt: number,
        createdAt: string,
        updatedAt: string,
      } | null > | null,
      nextToken: string | null,
      startedAt: number | null,
    } | null,
    _version: number,
    _deleted: boolean | null,
    _lastChangedAt: number,
    createdAt: string,
    updatedAt: string,
  } | null,
};

export type CreateJobPostMutationVariables = {
  input: CreateJobPostInput,
  condition?: ModelJobPostConditionInput | null,
};

export type CreateJobPostMutation = {
  createJobPost:  {
    __typename: "JobPost",
    id: string,
    owner: string,
    title: string,
    description: string,
    image: string,
    helpWantedID: string,
    helpWanted:  {
      __typename: "HelpWanted",
      id: string,
      owner: string,
      jobPosts:  {
        __typename: "ModelJobPostConnection",
        nextToken: string | null,
        startedAt: number | null,
      } | null,
      _version: number,
      _deleted: boolean | null,
      _lastChangedAt: number,
      createdAt: string,
      updatedAt: string,
    } | null,
    bids:  {
      __typename: "ModelBidConnection",
      items:  Array< {
        __typename: "Bid",
        id: string,
        owner: string,
        jobPostID: string,
        content: string,
        dollarValue: number,
        _version: number,
        _deleted: boolean | null,
        _lastChangedAt: number,
        createdAt: string,
        updatedAt: string,
      } | null > | null,
      nextToken: string | null,
      startedAt: number | null,
    } | null,
    _version: number,
    _deleted: boolean | null,
    _lastChangedAt: number,
    createdAt: string,
    updatedAt: string,
  } | null,
};

export type UpdateJobPostMutationVariables = {
  input: UpdateJobPostInput,
  condition?: ModelJobPostConditionInput | null,
};

export type UpdateJobPostMutation = {
  updateJobPost:  {
    __typename: "JobPost",
    id: string,
    owner: string,
    title: string,
    description: string,
    image: string,
    helpWantedID: string,
    helpWanted:  {
      __typename: "HelpWanted",
      id: string,
      owner: string,
      jobPosts:  {
        __typename: "ModelJobPostConnection",
        nextToken: string | null,
        startedAt: number | null,
      } | null,
      _version: number,
      _deleted: boolean | null,
      _lastChangedAt: number,
      createdAt: string,
      updatedAt: string,
    } | null,
    bids:  {
      __typename: "ModelBidConnection",
      items:  Array< {
        __typename: "Bid",
        id: string,
        owner: string,
        jobPostID: string,
        content: string,
        dollarValue: number,
        _version: number,
        _deleted: boolean | null,
        _lastChangedAt: number,
        createdAt: string,
        updatedAt: string,
      } | null > | null,
      nextToken: string | null,
      startedAt: number | null,
    } | null,
    _version: number,
    _deleted: boolean | null,
    _lastChangedAt: number,
    createdAt: string,
    updatedAt: string,
  } | null,
};

export type DeleteJobPostMutationVariables = {
  input: DeleteJobPostInput,
  condition?: ModelJobPostConditionInput | null,
};

export type DeleteJobPostMutation = {
  deleteJobPost:  {
    __typename: "JobPost",
    id: string,
    owner: string,
    title: string,
    description: string,
    image: string,
    helpWantedID: string,
    helpWanted:  {
      __typename: "HelpWanted",
      id: string,
      owner: string,
      jobPosts:  {
        __typename: "ModelJobPostConnection",
        nextToken: string | null,
        startedAt: number | null,
      } | null,
      _version: number,
      _deleted: boolean | null,
      _lastChangedAt: number,
      createdAt: string,
      updatedAt: string,
    } | null,
    bids:  {
      __typename: "ModelBidConnection",
      items:  Array< {
        __typename: "Bid",
        id: string,
        owner: string,
        jobPostID: string,
        content: string,
        dollarValue: number,
        _version: number,
        _deleted: boolean | null,
        _lastChangedAt: number,
        createdAt: string,
        updatedAt: string,
      } | null > | null,
      nextToken: string | null,
      startedAt: number | null,
    } | null,
    _version: number,
    _deleted: boolean | null,
    _lastChangedAt: number,
    createdAt: string,
    updatedAt: string,
  } | null,
};

export type CreateBidMutationVariables = {
  input: CreateBidInput,
  condition?: ModelBidConditionInput | null,
};

export type CreateBidMutation = {
  createBid:  {
    __typename: "Bid",
    id: string,
    owner: string,
    jobPostID: string,
    jobPost:  {
      __typename: "JobPost",
      id: string,
      owner: string,
      title: string,
      description: string,
      image: string,
      helpWantedID: string,
      helpWanted:  {
        __typename: "HelpWanted",
        id: string,
        owner: string,
        _version: number,
        _deleted: boolean | null,
        _lastChangedAt: number,
        createdAt: string,
        updatedAt: string,
      } | null,
      bids:  {
        __typename: "ModelBidConnection",
        nextToken: string | null,
        startedAt: number | null,
      } | null,
      _version: number,
      _deleted: boolean | null,
      _lastChangedAt: number,
      createdAt: string,
      updatedAt: string,
    } | null,
    content: string,
    dollarValue: number,
    _version: number,
    _deleted: boolean | null,
    _lastChangedAt: number,
    createdAt: string,
    updatedAt: string,
  } | null,
};

export type UpdateBidMutationVariables = {
  input: UpdateBidInput,
  condition?: ModelBidConditionInput | null,
};

export type UpdateBidMutation = {
  updateBid:  {
    __typename: "Bid",
    id: string,
    owner: string,
    jobPostID: string,
    jobPost:  {
      __typename: "JobPost",
      id: string,
      owner: string,
      title: string,
      description: string,
      image: string,
      helpWantedID: string,
      helpWanted:  {
        __typename: "HelpWanted",
        id: string,
        owner: string,
        _version: number,
        _deleted: boolean | null,
        _lastChangedAt: number,
        createdAt: string,
        updatedAt: string,
      } | null,
      bids:  {
        __typename: "ModelBidConnection",
        nextToken: string | null,
        startedAt: number | null,
      } | null,
      _version: number,
      _deleted: boolean | null,
      _lastChangedAt: number,
      createdAt: string,
      updatedAt: string,
    } | null,
    content: string,
    dollarValue: number,
    _version: number,
    _deleted: boolean | null,
    _lastChangedAt: number,
    createdAt: string,
    updatedAt: string,
  } | null,
};

export type DeleteBidMutationVariables = {
  input: DeleteBidInput,
  condition?: ModelBidConditionInput | null,
};

export type DeleteBidMutation = {
  deleteBid:  {
    __typename: "Bid",
    id: string,
    owner: string,
    jobPostID: string,
    jobPost:  {
      __typename: "JobPost",
      id: string,
      owner: string,
      title: string,
      description: string,
      image: string,
      helpWantedID: string,
      helpWanted:  {
        __typename: "HelpWanted",
        id: string,
        owner: string,
        _version: number,
        _deleted: boolean | null,
        _lastChangedAt: number,
        createdAt: string,
        updatedAt: string,
      } | null,
      bids:  {
        __typename: "ModelBidConnection",
        nextToken: string | null,
        startedAt: number | null,
      } | null,
      _version: number,
      _deleted: boolean | null,
      _lastChangedAt: number,
      createdAt: string,
      updatedAt: string,
    } | null,
    content: string,
    dollarValue: number,
    _version: number,
    _deleted: boolean | null,
    _lastChangedAt: number,
    createdAt: string,
    updatedAt: string,
  } | null,
};

export type SyncHelpWantedsQueryVariables = {
  filter?: ModelHelpWantedFilterInput | null,
  limit?: number | null,
  nextToken?: string | null,
  lastSync?: number | null,
};

export type SyncHelpWantedsQuery = {
  syncHelpWanteds:  {
    __typename: "ModelHelpWantedConnection",
    items:  Array< {
      __typename: "HelpWanted",
      id: string,
      owner: string,
      jobPosts:  {
        __typename: "ModelJobPostConnection",
        nextToken: string | null,
        startedAt: number | null,
      } | null,
      _version: number,
      _deleted: boolean | null,
      _lastChangedAt: number,
      createdAt: string,
      updatedAt: string,
    } | null > | null,
    nextToken: string | null,
    startedAt: number | null,
  } | null,
};

export type GetHelpWantedQueryVariables = {
  id: string,
};

export type GetHelpWantedQuery = {
  getHelpWanted:  {
    __typename: "HelpWanted",
    id: string,
    owner: string,
    jobPosts:  {
      __typename: "ModelJobPostConnection",
      items:  Array< {
        __typename: "JobPost",
        id: string,
        owner: string,
        title: string,
        description: string,
        image: string,
        helpWantedID: string,
        _version: number,
        _deleted: boolean | null,
        _lastChangedAt: number,
        createdAt: string,
        updatedAt: string,
      } | null > | null,
      nextToken: string | null,
      startedAt: number | null,
    } | null,
    _version: number,
    _deleted: boolean | null,
    _lastChangedAt: number,
    createdAt: string,
    updatedAt: string,
  } | null,
};

export type ListHelpWantedsQueryVariables = {
  filter?: ModelHelpWantedFilterInput | null,
  limit?: number | null,
  nextToken?: string | null,
};

export type ListHelpWantedsQuery = {
  listHelpWanteds:  {
    __typename: "ModelHelpWantedConnection",
    items:  Array< {
      __typename: "HelpWanted",
      id: string,
      owner: string,
      jobPosts:  {
        __typename: "ModelJobPostConnection",
        nextToken: string | null,
        startedAt: number | null,
      } | null,
      _version: number,
      _deleted: boolean | null,
      _lastChangedAt: number,
      createdAt: string,
      updatedAt: string,
    } | null > | null,
    nextToken: string | null,
    startedAt: number | null,
  } | null,
};

export type SyncJobPostsQueryVariables = {
  filter?: ModelJobPostFilterInput | null,
  limit?: number | null,
  nextToken?: string | null,
  lastSync?: number | null,
};

export type SyncJobPostsQuery = {
  syncJobPosts:  {
    __typename: "ModelJobPostConnection",
    items:  Array< {
      __typename: "JobPost",
      id: string,
      owner: string,
      title: string,
      description: string,
      image: string,
      helpWantedID: string,
      helpWanted:  {
        __typename: "HelpWanted",
        id: string,
        owner: string,
        _version: number,
        _deleted: boolean | null,
        _lastChangedAt: number,
        createdAt: string,
        updatedAt: string,
      } | null,
      bids:  {
        __typename: "ModelBidConnection",
        nextToken: string | null,
        startedAt: number | null,
      } | null,
      _version: number,
      _deleted: boolean | null,
      _lastChangedAt: number,
      createdAt: string,
      updatedAt: string,
    } | null > | null,
    nextToken: string | null,
    startedAt: number | null,
  } | null,
};

export type GetJobPostQueryVariables = {
  id: string,
};

export type GetJobPostQuery = {
  getJobPost:  {
    __typename: "JobPost",
    id: string,
    owner: string,
    title: string,
    description: string,
    image: string,
    helpWantedID: string,
    helpWanted:  {
      __typename: "HelpWanted",
      id: string,
      owner: string,
      jobPosts:  {
        __typename: "ModelJobPostConnection",
        nextToken: string | null,
        startedAt: number | null,
      } | null,
      _version: number,
      _deleted: boolean | null,
      _lastChangedAt: number,
      createdAt: string,
      updatedAt: string,
    } | null,
    bids:  {
      __typename: "ModelBidConnection",
      items:  Array< {
        __typename: "Bid",
        id: string,
        owner: string,
        jobPostID: string,
        content: string,
        dollarValue: number,
        _version: number,
        _deleted: boolean | null,
        _lastChangedAt: number,
        createdAt: string,
        updatedAt: string,
      } | null > | null,
      nextToken: string | null,
      startedAt: number | null,
    } | null,
    _version: number,
    _deleted: boolean | null,
    _lastChangedAt: number,
    createdAt: string,
    updatedAt: string,
  } | null,
};

export type ListJobPostsQueryVariables = {
  filter?: ModelJobPostFilterInput | null,
  limit?: number | null,
  nextToken?: string | null,
};

export type ListJobPostsQuery = {
  listJobPosts:  {
    __typename: "ModelJobPostConnection",
    items:  Array< {
      __typename: "JobPost",
      id: string,
      owner: string,
      title: string,
      description: string,
      image: string,
      helpWantedID: string,
      helpWanted:  {
        __typename: "HelpWanted",
        id: string,
        owner: string,
        _version: number,
        _deleted: boolean | null,
        _lastChangedAt: number,
        createdAt: string,
        updatedAt: string,
      } | null,
      bids:  {
        __typename: "ModelBidConnection",
        nextToken: string | null,
        startedAt: number | null,
      } | null,
      _version: number,
      _deleted: boolean | null,
      _lastChangedAt: number,
      createdAt: string,
      updatedAt: string,
    } | null > | null,
    nextToken: string | null,
    startedAt: number | null,
  } | null,
};

export type SyncBidsQueryVariables = {
  filter?: ModelBidFilterInput | null,
  limit?: number | null,
  nextToken?: string | null,
  lastSync?: number | null,
};

export type SyncBidsQuery = {
  syncBids:  {
    __typename: "ModelBidConnection",
    items:  Array< {
      __typename: "Bid",
      id: string,
      owner: string,
      jobPostID: string,
      jobPost:  {
        __typename: "JobPost",
        id: string,
        owner: string,
        title: string,
        description: string,
        image: string,
        helpWantedID: string,
        _version: number,
        _deleted: boolean | null,
        _lastChangedAt: number,
        createdAt: string,
        updatedAt: string,
      } | null,
      content: string,
      dollarValue: number,
      _version: number,
      _deleted: boolean | null,
      _lastChangedAt: number,
      createdAt: string,
      updatedAt: string,
    } | null > | null,
    nextToken: string | null,
    startedAt: number | null,
  } | null,
};

export type GetBidQueryVariables = {
  id: string,
};

export type GetBidQuery = {
  getBid:  {
    __typename: "Bid",
    id: string,
    owner: string,
    jobPostID: string,
    jobPost:  {
      __typename: "JobPost",
      id: string,
      owner: string,
      title: string,
      description: string,
      image: string,
      helpWantedID: string,
      helpWanted:  {
        __typename: "HelpWanted",
        id: string,
        owner: string,
        _version: number,
        _deleted: boolean | null,
        _lastChangedAt: number,
        createdAt: string,
        updatedAt: string,
      } | null,
      bids:  {
        __typename: "ModelBidConnection",
        nextToken: string | null,
        startedAt: number | null,
      } | null,
      _version: number,
      _deleted: boolean | null,
      _lastChangedAt: number,
      createdAt: string,
      updatedAt: string,
    } | null,
    content: string,
    dollarValue: number,
    _version: number,
    _deleted: boolean | null,
    _lastChangedAt: number,
    createdAt: string,
    updatedAt: string,
  } | null,
};

export type ListBidsQueryVariables = {
  filter?: ModelBidFilterInput | null,
  limit?: number | null,
  nextToken?: string | null,
};

export type ListBidsQuery = {
  listBids:  {
    __typename: "ModelBidConnection",
    items:  Array< {
      __typename: "Bid",
      id: string,
      owner: string,
      jobPostID: string,
      jobPost:  {
        __typename: "JobPost",
        id: string,
        owner: string,
        title: string,
        description: string,
        image: string,
        helpWantedID: string,
        _version: number,
        _deleted: boolean | null,
        _lastChangedAt: number,
        createdAt: string,
        updatedAt: string,
      } | null,
      content: string,
      dollarValue: number,
      _version: number,
      _deleted: boolean | null,
      _lastChangedAt: number,
      createdAt: string,
      updatedAt: string,
    } | null > | null,
    nextToken: string | null,
    startedAt: number | null,
  } | null,
};

export type OnCreateHelpWantedSubscriptionVariables = {
  owner: string,
};

export type OnCreateHelpWantedSubscription = {
  onCreateHelpWanted:  {
    __typename: "HelpWanted",
    id: string,
    owner: string,
    jobPosts:  {
      __typename: "ModelJobPostConnection",
      items:  Array< {
        __typename: "JobPost",
        id: string,
        owner: string,
        title: string,
        description: string,
        image: string,
        helpWantedID: string,
        _version: number,
        _deleted: boolean | null,
        _lastChangedAt: number,
        createdAt: string,
        updatedAt: string,
      } | null > | null,
      nextToken: string | null,
      startedAt: number | null,
    } | null,
    _version: number,
    _deleted: boolean | null,
    _lastChangedAt: number,
    createdAt: string,
    updatedAt: string,
  } | null,
};

export type OnUpdateHelpWantedSubscriptionVariables = {
  owner: string,
};

export type OnUpdateHelpWantedSubscription = {
  onUpdateHelpWanted:  {
    __typename: "HelpWanted",
    id: string,
    owner: string,
    jobPosts:  {
      __typename: "ModelJobPostConnection",
      items:  Array< {
        __typename: "JobPost",
        id: string,
        owner: string,
        title: string,
        description: string,
        image: string,
        helpWantedID: string,
        _version: number,
        _deleted: boolean | null,
        _lastChangedAt: number,
        createdAt: string,
        updatedAt: string,
      } | null > | null,
      nextToken: string | null,
      startedAt: number | null,
    } | null,
    _version: number,
    _deleted: boolean | null,
    _lastChangedAt: number,
    createdAt: string,
    updatedAt: string,
  } | null,
};

export type OnDeleteHelpWantedSubscriptionVariables = {
  owner: string,
};

export type OnDeleteHelpWantedSubscription = {
  onDeleteHelpWanted:  {
    __typename: "HelpWanted",
    id: string,
    owner: string,
    jobPosts:  {
      __typename: "ModelJobPostConnection",
      items:  Array< {
        __typename: "JobPost",
        id: string,
        owner: string,
        title: string,
        description: string,
        image: string,
        helpWantedID: string,
        _version: number,
        _deleted: boolean | null,
        _lastChangedAt: number,
        createdAt: string,
        updatedAt: string,
      } | null > | null,
      nextToken: string | null,
      startedAt: number | null,
    } | null,
    _version: number,
    _deleted: boolean | null,
    _lastChangedAt: number,
    createdAt: string,
    updatedAt: string,
  } | null,
};

export type OnCreateJobPostSubscription = {
  onCreateJobPost:  {
    __typename: "JobPost",
    id: string,
    owner: string,
    title: string,
    description: string,
    image: string,
    helpWantedID: string,
    helpWanted:  {
      __typename: "HelpWanted",
      id: string,
      owner: string,
      jobPosts:  {
        __typename: "ModelJobPostConnection",
        nextToken: string | null,
        startedAt: number | null,
      } | null,
      _version: number,
      _deleted: boolean | null,
      _lastChangedAt: number,
      createdAt: string,
      updatedAt: string,
    } | null,
    bids:  {
      __typename: "ModelBidConnection",
      items:  Array< {
        __typename: "Bid",
        id: string,
        owner: string,
        jobPostID: string,
        content: string,
        dollarValue: number,
        _version: number,
        _deleted: boolean | null,
        _lastChangedAt: number,
        createdAt: string,
        updatedAt: string,
      } | null > | null,
      nextToken: string | null,
      startedAt: number | null,
    } | null,
    _version: number,
    _deleted: boolean | null,
    _lastChangedAt: number,
    createdAt: string,
    updatedAt: string,
  } | null,
};

export type OnUpdateJobPostSubscription = {
  onUpdateJobPost:  {
    __typename: "JobPost",
    id: string,
    owner: string,
    title: string,
    description: string,
    image: string,
    helpWantedID: string,
    helpWanted:  {
      __typename: "HelpWanted",
      id: string,
      owner: string,
      jobPosts:  {
        __typename: "ModelJobPostConnection",
        nextToken: string | null,
        startedAt: number | null,
      } | null,
      _version: number,
      _deleted: boolean | null,
      _lastChangedAt: number,
      createdAt: string,
      updatedAt: string,
    } | null,
    bids:  {
      __typename: "ModelBidConnection",
      items:  Array< {
        __typename: "Bid",
        id: string,
        owner: string,
        jobPostID: string,
        content: string,
        dollarValue: number,
        _version: number,
        _deleted: boolean | null,
        _lastChangedAt: number,
        createdAt: string,
        updatedAt: string,
      } | null > | null,
      nextToken: string | null,
      startedAt: number | null,
    } | null,
    _version: number,
    _deleted: boolean | null,
    _lastChangedAt: number,
    createdAt: string,
    updatedAt: string,
  } | null,
};

export type OnDeleteJobPostSubscription = {
  onDeleteJobPost:  {
    __typename: "JobPost",
    id: string,
    owner: string,
    title: string,
    description: string,
    image: string,
    helpWantedID: string,
    helpWanted:  {
      __typename: "HelpWanted",
      id: string,
      owner: string,
      jobPosts:  {
        __typename: "ModelJobPostConnection",
        nextToken: string | null,
        startedAt: number | null,
      } | null,
      _version: number,
      _deleted: boolean | null,
      _lastChangedAt: number,
      createdAt: string,
      updatedAt: string,
    } | null,
    bids:  {
      __typename: "ModelBidConnection",
      items:  Array< {
        __typename: "Bid",
        id: string,
        owner: string,
        jobPostID: string,
        content: string,
        dollarValue: number,
        _version: number,
        _deleted: boolean | null,
        _lastChangedAt: number,
        createdAt: string,
        updatedAt: string,
      } | null > | null,
      nextToken: string | null,
      startedAt: number | null,
    } | null,
    _version: number,
    _deleted: boolean | null,
    _lastChangedAt: number,
    createdAt: string,
    updatedAt: string,
  } | null,
};

export type OnCreateBidSubscription = {
  onCreateBid:  {
    __typename: "Bid",
    id: string,
    owner: string,
    jobPostID: string,
    jobPost:  {
      __typename: "JobPost",
      id: string,
      owner: string,
      title: string,
      description: string,
      image: string,
      helpWantedID: string,
      helpWanted:  {
        __typename: "HelpWanted",
        id: string,
        owner: string,
        _version: number,
        _deleted: boolean | null,
        _lastChangedAt: number,
        createdAt: string,
        updatedAt: string,
      } | null,
      bids:  {
        __typename: "ModelBidConnection",
        nextToken: string | null,
        startedAt: number | null,
      } | null,
      _version: number,
      _deleted: boolean | null,
      _lastChangedAt: number,
      createdAt: string,
      updatedAt: string,
    } | null,
    content: string,
    dollarValue: number,
    _version: number,
    _deleted: boolean | null,
    _lastChangedAt: number,
    createdAt: string,
    updatedAt: string,
  } | null,
};

export type OnUpdateBidSubscription = {
  onUpdateBid:  {
    __typename: "Bid",
    id: string,
    owner: string,
    jobPostID: string,
    jobPost:  {
      __typename: "JobPost",
      id: string,
      owner: string,
      title: string,
      description: string,
      image: string,
      helpWantedID: string,
      helpWanted:  {
        __typename: "HelpWanted",
        id: string,
        owner: string,
        _version: number,
        _deleted: boolean | null,
        _lastChangedAt: number,
        createdAt: string,
        updatedAt: string,
      } | null,
      bids:  {
        __typename: "ModelBidConnection",
        nextToken: string | null,
        startedAt: number | null,
      } | null,
      _version: number,
      _deleted: boolean | null,
      _lastChangedAt: number,
      createdAt: string,
      updatedAt: string,
    } | null,
    content: string,
    dollarValue: number,
    _version: number,
    _deleted: boolean | null,
    _lastChangedAt: number,
    createdAt: string,
    updatedAt: string,
  } | null,
};

export type OnDeleteBidSubscription = {
  onDeleteBid:  {
    __typename: "Bid",
    id: string,
    owner: string,
    jobPostID: string,
    jobPost:  {
      __typename: "JobPost",
      id: string,
      owner: string,
      title: string,
      description: string,
      image: string,
      helpWantedID: string,
      helpWanted:  {
        __typename: "HelpWanted",
        id: string,
        owner: string,
        _version: number,
        _deleted: boolean | null,
        _lastChangedAt: number,
        createdAt: string,
        updatedAt: string,
      } | null,
      bids:  {
        __typename: "ModelBidConnection",
        nextToken: string | null,
        startedAt: number | null,
      } | null,
      _version: number,
      _deleted: boolean | null,
      _lastChangedAt: number,
      createdAt: string,
      updatedAt: string,
    } | null,
    content: string,
    dollarValue: number,
    _version: number,
    _deleted: boolean | null,
    _lastChangedAt: number,
    createdAt: string,
    updatedAt: string,
  } | null,
};
