// @ts-check
import { initSchema } from '@aws-amplify/datastore';
import { schema } from './schema';



const { HelpWanted, JobPost, Bid } = initSchema(schema);

export {
  HelpWanted,
  JobPost,
  Bid
};