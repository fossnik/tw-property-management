import { ModelInit, MutableModel, PersistentModelConstructor } from "@aws-amplify/datastore";





export declare class HelpWanted {
  readonly id: string;
  readonly owner: string;
  readonly jobPosts?: JobPost[];
  constructor(init: ModelInit<HelpWanted>);
  static copyOf(source: HelpWanted, mutator: (draft: MutableModel<HelpWanted>) => MutableModel<HelpWanted> | void): HelpWanted;
}

export declare class JobPost {
  readonly id: string;
  readonly owner: string;
  readonly title: string;
  readonly description: string;
  readonly image: string;
  readonly helpWanted?: HelpWanted;
  readonly bids?: Bid[];
  constructor(init: ModelInit<JobPost>);
  static copyOf(source: JobPost, mutator: (draft: MutableModel<JobPost>) => MutableModel<JobPost> | void): JobPost;
}

export declare class Bid {
  readonly id: string;
  readonly owner: string;
  readonly jobPost?: JobPost;
  readonly content: string;
  readonly dollarValue: number;
  constructor(init: ModelInit<Bid>);
  static copyOf(source: Bid, mutator: (draft: MutableModel<Bid>) => MutableModel<Bid> | void): Bid;
}