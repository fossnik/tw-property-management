/* tslint:disable */
/* eslint-disable */
// this is an auto generated file. This will be overwritten

export const createHelpWanted = /* GraphQL */ `
  mutation CreateHelpWanted(
    $input: CreateHelpWantedInput!
    $condition: ModelHelpWantedConditionInput
  ) {
    createHelpWanted(input: $input, condition: $condition) {
      id
      owner
      jobPosts {
        items {
          id
          owner
          title
          description
          image
          helpWantedID
          _version
          _deleted
          _lastChangedAt
          createdAt
          updatedAt
        }
        nextToken
        startedAt
      }
      _version
      _deleted
      _lastChangedAt
      createdAt
      updatedAt
    }
  }
`;
export const updateHelpWanted = /* GraphQL */ `
  mutation UpdateHelpWanted(
    $input: UpdateHelpWantedInput!
    $condition: ModelHelpWantedConditionInput
  ) {
    updateHelpWanted(input: $input, condition: $condition) {
      id
      owner
      jobPosts {
        items {
          id
          owner
          title
          description
          image
          helpWantedID
          _version
          _deleted
          _lastChangedAt
          createdAt
          updatedAt
        }
        nextToken
        startedAt
      }
      _version
      _deleted
      _lastChangedAt
      createdAt
      updatedAt
    }
  }
`;
export const deleteHelpWanted = /* GraphQL */ `
  mutation DeleteHelpWanted(
    $input: DeleteHelpWantedInput!
    $condition: ModelHelpWantedConditionInput
  ) {
    deleteHelpWanted(input: $input, condition: $condition) {
      id
      owner
      jobPosts {
        items {
          id
          owner
          title
          description
          image
          helpWantedID
          _version
          _deleted
          _lastChangedAt
          createdAt
          updatedAt
        }
        nextToken
        startedAt
      }
      _version
      _deleted
      _lastChangedAt
      createdAt
      updatedAt
    }
  }
`;
export const createJobPost = /* GraphQL */ `
  mutation CreateJobPost(
    $input: CreateJobPostInput!
    $condition: ModelJobPostConditionInput
  ) {
    createJobPost(input: $input, condition: $condition) {
      id
      owner
      title
      description
      image
      helpWantedID
      helpWanted {
        id
        owner
        jobPosts {
          nextToken
          startedAt
        }
        _version
        _deleted
        _lastChangedAt
        createdAt
        updatedAt
      }
      bids {
        items {
          id
          owner
          jobPostID
          content
          dollarValue
          _version
          _deleted
          _lastChangedAt
          createdAt
          updatedAt
        }
        nextToken
        startedAt
      }
      _version
      _deleted
      _lastChangedAt
      createdAt
      updatedAt
    }
  }
`;
export const updateJobPost = /* GraphQL */ `
  mutation UpdateJobPost(
    $input: UpdateJobPostInput!
    $condition: ModelJobPostConditionInput
  ) {
    updateJobPost(input: $input, condition: $condition) {
      id
      owner
      title
      description
      image
      helpWantedID
      helpWanted {
        id
        owner
        jobPosts {
          nextToken
          startedAt
        }
        _version
        _deleted
        _lastChangedAt
        createdAt
        updatedAt
      }
      bids {
        items {
          id
          owner
          jobPostID
          content
          dollarValue
          _version
          _deleted
          _lastChangedAt
          createdAt
          updatedAt
        }
        nextToken
        startedAt
      }
      _version
      _deleted
      _lastChangedAt
      createdAt
      updatedAt
    }
  }
`;
export const deleteJobPost = /* GraphQL */ `
  mutation DeleteJobPost(
    $input: DeleteJobPostInput!
    $condition: ModelJobPostConditionInput
  ) {
    deleteJobPost(input: $input, condition: $condition) {
      id
      owner
      title
      description
      image
      helpWantedID
      helpWanted {
        id
        owner
        jobPosts {
          nextToken
          startedAt
        }
        _version
        _deleted
        _lastChangedAt
        createdAt
        updatedAt
      }
      bids {
        items {
          id
          owner
          jobPostID
          content
          dollarValue
          _version
          _deleted
          _lastChangedAt
          createdAt
          updatedAt
        }
        nextToken
        startedAt
      }
      _version
      _deleted
      _lastChangedAt
      createdAt
      updatedAt
    }
  }
`;
export const createBid = /* GraphQL */ `
  mutation CreateBid(
    $input: CreateBidInput!
    $condition: ModelBidConditionInput
  ) {
    createBid(input: $input, condition: $condition) {
      id
      owner
      jobPostID
      jobPost {
        id
        owner
        title
        description
        image
        helpWantedID
        helpWanted {
          id
          owner
          _version
          _deleted
          _lastChangedAt
          createdAt
          updatedAt
        }
        bids {
          nextToken
          startedAt
        }
        _version
        _deleted
        _lastChangedAt
        createdAt
        updatedAt
      }
      content
      dollarValue
      _version
      _deleted
      _lastChangedAt
      createdAt
      updatedAt
    }
  }
`;
export const updateBid = /* GraphQL */ `
  mutation UpdateBid(
    $input: UpdateBidInput!
    $condition: ModelBidConditionInput
  ) {
    updateBid(input: $input, condition: $condition) {
      id
      owner
      jobPostID
      jobPost {
        id
        owner
        title
        description
        image
        helpWantedID
        helpWanted {
          id
          owner
          _version
          _deleted
          _lastChangedAt
          createdAt
          updatedAt
        }
        bids {
          nextToken
          startedAt
        }
        _version
        _deleted
        _lastChangedAt
        createdAt
        updatedAt
      }
      content
      dollarValue
      _version
      _deleted
      _lastChangedAt
      createdAt
      updatedAt
    }
  }
`;
export const deleteBid = /* GraphQL */ `
  mutation DeleteBid(
    $input: DeleteBidInput!
    $condition: ModelBidConditionInput
  ) {
    deleteBid(input: $input, condition: $condition) {
      id
      owner
      jobPostID
      jobPost {
        id
        owner
        title
        description
        image
        helpWantedID
        helpWanted {
          id
          owner
          _version
          _deleted
          _lastChangedAt
          createdAt
          updatedAt
        }
        bids {
          nextToken
          startedAt
        }
        _version
        _deleted
        _lastChangedAt
        createdAt
        updatedAt
      }
      content
      dollarValue
      _version
      _deleted
      _lastChangedAt
      createdAt
      updatedAt
    }
  }
`;
