/* tslint:disable */
/* eslint-disable */
// this is an auto generated file. This will be overwritten

export const syncHelpWanteds = /* GraphQL */ `
  query SyncHelpWanteds(
    $filter: ModelHelpWantedFilterInput
    $limit: Int
    $nextToken: String
    $lastSync: AWSTimestamp
  ) {
    syncHelpWanteds(
      filter: $filter
      limit: $limit
      nextToken: $nextToken
      lastSync: $lastSync
    ) {
      items {
        id
        owner
        jobPosts {
          nextToken
          startedAt
        }
        _version
        _deleted
        _lastChangedAt
        createdAt
        updatedAt
      }
      nextToken
      startedAt
    }
  }
`;
export const getHelpWanted = /* GraphQL */ `
  query GetHelpWanted($id: ID!) {
    getHelpWanted(id: $id) {
      id
      owner
      jobPosts {
        items {
          id
          owner
          title
          description
          image
          helpWantedID
          _version
          _deleted
          _lastChangedAt
          createdAt
          updatedAt
        }
        nextToken
        startedAt
      }
      _version
      _deleted
      _lastChangedAt
      createdAt
      updatedAt
    }
  }
`;
export const listHelpWanteds = /* GraphQL */ `
  query ListHelpWanteds(
    $filter: ModelHelpWantedFilterInput
    $limit: Int
    $nextToken: String
  ) {
    listHelpWanteds(filter: $filter, limit: $limit, nextToken: $nextToken) {
      items {
        id
        owner
        jobPosts {
          nextToken
          startedAt
        }
        _version
        _deleted
        _lastChangedAt
        createdAt
        updatedAt
      }
      nextToken
      startedAt
    }
  }
`;
export const syncJobPosts = /* GraphQL */ `
  query SyncJobPosts(
    $filter: ModelJobPostFilterInput
    $limit: Int
    $nextToken: String
    $lastSync: AWSTimestamp
  ) {
    syncJobPosts(
      filter: $filter
      limit: $limit
      nextToken: $nextToken
      lastSync: $lastSync
    ) {
      items {
        id
        owner
        title
        description
        image
        helpWantedID
        helpWanted {
          id
          owner
          _version
          _deleted
          _lastChangedAt
          createdAt
          updatedAt
        }
        bids {
          nextToken
          startedAt
        }
        _version
        _deleted
        _lastChangedAt
        createdAt
        updatedAt
      }
      nextToken
      startedAt
    }
  }
`;
export const getJobPost = /* GraphQL */ `
  query GetJobPost($id: ID!) {
    getJobPost(id: $id) {
      id
      owner
      title
      description
      image
      helpWantedID
      helpWanted {
        id
        owner
        jobPosts {
          nextToken
          startedAt
        }
        _version
        _deleted
        _lastChangedAt
        createdAt
        updatedAt
      }
      bids {
        items {
          id
          owner
          jobPostID
          content
          dollarValue
          _version
          _deleted
          _lastChangedAt
          createdAt
          updatedAt
        }
        nextToken
        startedAt
      }
      _version
      _deleted
      _lastChangedAt
      createdAt
      updatedAt
    }
  }
`;
export const listJobPosts = /* GraphQL */ `
  query ListJobPosts(
    $filter: ModelJobPostFilterInput
    $limit: Int
    $nextToken: String
  ) {
    listJobPosts(filter: $filter, limit: $limit, nextToken: $nextToken) {
      items {
        id
        owner
        title
        description
        image
        helpWantedID
        helpWanted {
          id
          owner
          _version
          _deleted
          _lastChangedAt
          createdAt
          updatedAt
        }
        bids {
          nextToken
          startedAt
        }
        _version
        _deleted
        _lastChangedAt
        createdAt
        updatedAt
      }
      nextToken
      startedAt
    }
  }
`;
export const syncBids = /* GraphQL */ `
  query SyncBids(
    $filter: ModelBidFilterInput
    $limit: Int
    $nextToken: String
    $lastSync: AWSTimestamp
  ) {
    syncBids(
      filter: $filter
      limit: $limit
      nextToken: $nextToken
      lastSync: $lastSync
    ) {
      items {
        id
        owner
        jobPostID
        jobPost {
          id
          owner
          title
          description
          image
          helpWantedID
          _version
          _deleted
          _lastChangedAt
          createdAt
          updatedAt
        }
        content
        dollarValue
        _version
        _deleted
        _lastChangedAt
        createdAt
        updatedAt
      }
      nextToken
      startedAt
    }
  }
`;
export const getBid = /* GraphQL */ `
  query GetBid($id: ID!) {
    getBid(id: $id) {
      id
      owner
      jobPostID
      jobPost {
        id
        owner
        title
        description
        image
        helpWantedID
        helpWanted {
          id
          owner
          _version
          _deleted
          _lastChangedAt
          createdAt
          updatedAt
        }
        bids {
          nextToken
          startedAt
        }
        _version
        _deleted
        _lastChangedAt
        createdAt
        updatedAt
      }
      content
      dollarValue
      _version
      _deleted
      _lastChangedAt
      createdAt
      updatedAt
    }
  }
`;
export const listBids = /* GraphQL */ `
  query ListBids(
    $filter: ModelBidFilterInput
    $limit: Int
    $nextToken: String
  ) {
    listBids(filter: $filter, limit: $limit, nextToken: $nextToken) {
      items {
        id
        owner
        jobPostID
        jobPost {
          id
          owner
          title
          description
          image
          helpWantedID
          _version
          _deleted
          _lastChangedAt
          createdAt
          updatedAt
        }
        content
        dollarValue
        _version
        _deleted
        _lastChangedAt
        createdAt
        updatedAt
      }
      nextToken
      startedAt
    }
  }
`;
