/* tslint:disable */
/* eslint-disable */
// this is an auto generated file. This will be overwritten

export const onCreateHelpWanted = /* GraphQL */ `
  subscription OnCreateHelpWanted($owner: String!) {
    onCreateHelpWanted(owner: $owner) {
      id
      owner
      jobPosts {
        items {
          id
          owner
          title
          description
          image
          helpWantedID
          _version
          _deleted
          _lastChangedAt
          createdAt
          updatedAt
        }
        nextToken
        startedAt
      }
      _version
      _deleted
      _lastChangedAt
      createdAt
      updatedAt
    }
  }
`;
export const onUpdateHelpWanted = /* GraphQL */ `
  subscription OnUpdateHelpWanted($owner: String!) {
    onUpdateHelpWanted(owner: $owner) {
      id
      owner
      jobPosts {
        items {
          id
          owner
          title
          description
          image
          helpWantedID
          _version
          _deleted
          _lastChangedAt
          createdAt
          updatedAt
        }
        nextToken
        startedAt
      }
      _version
      _deleted
      _lastChangedAt
      createdAt
      updatedAt
    }
  }
`;
export const onDeleteHelpWanted = /* GraphQL */ `
  subscription OnDeleteHelpWanted($owner: String!) {
    onDeleteHelpWanted(owner: $owner) {
      id
      owner
      jobPosts {
        items {
          id
          owner
          title
          description
          image
          helpWantedID
          _version
          _deleted
          _lastChangedAt
          createdAt
          updatedAt
        }
        nextToken
        startedAt
      }
      _version
      _deleted
      _lastChangedAt
      createdAt
      updatedAt
    }
  }
`;
export const onCreateJobPost = /* GraphQL */ `
  subscription OnCreateJobPost {
    onCreateJobPost {
      id
      owner
      title
      description
      image
      helpWantedID
      helpWanted {
        id
        owner
        jobPosts {
          nextToken
          startedAt
        }
        _version
        _deleted
        _lastChangedAt
        createdAt
        updatedAt
      }
      bids {
        items {
          id
          owner
          jobPostID
          content
          dollarValue
          _version
          _deleted
          _lastChangedAt
          createdAt
          updatedAt
        }
        nextToken
        startedAt
      }
      _version
      _deleted
      _lastChangedAt
      createdAt
      updatedAt
    }
  }
`;
export const onUpdateJobPost = /* GraphQL */ `
  subscription OnUpdateJobPost {
    onUpdateJobPost {
      id
      owner
      title
      description
      image
      helpWantedID
      helpWanted {
        id
        owner
        jobPosts {
          nextToken
          startedAt
        }
        _version
        _deleted
        _lastChangedAt
        createdAt
        updatedAt
      }
      bids {
        items {
          id
          owner
          jobPostID
          content
          dollarValue
          _version
          _deleted
          _lastChangedAt
          createdAt
          updatedAt
        }
        nextToken
        startedAt
      }
      _version
      _deleted
      _lastChangedAt
      createdAt
      updatedAt
    }
  }
`;
export const onDeleteJobPost = /* GraphQL */ `
  subscription OnDeleteJobPost {
    onDeleteJobPost {
      id
      owner
      title
      description
      image
      helpWantedID
      helpWanted {
        id
        owner
        jobPosts {
          nextToken
          startedAt
        }
        _version
        _deleted
        _lastChangedAt
        createdAt
        updatedAt
      }
      bids {
        items {
          id
          owner
          jobPostID
          content
          dollarValue
          _version
          _deleted
          _lastChangedAt
          createdAt
          updatedAt
        }
        nextToken
        startedAt
      }
      _version
      _deleted
      _lastChangedAt
      createdAt
      updatedAt
    }
  }
`;
export const onCreateBid = /* GraphQL */ `
  subscription OnCreateBid {
    onCreateBid {
      id
      owner
      jobPostID
      jobPost {
        id
        owner
        title
        description
        image
        helpWantedID
        helpWanted {
          id
          owner
          _version
          _deleted
          _lastChangedAt
          createdAt
          updatedAt
        }
        bids {
          nextToken
          startedAt
        }
        _version
        _deleted
        _lastChangedAt
        createdAt
        updatedAt
      }
      content
      dollarValue
      _version
      _deleted
      _lastChangedAt
      createdAt
      updatedAt
    }
  }
`;
export const onUpdateBid = /* GraphQL */ `
  subscription OnUpdateBid {
    onUpdateBid {
      id
      owner
      jobPostID
      jobPost {
        id
        owner
        title
        description
        image
        helpWantedID
        helpWanted {
          id
          owner
          _version
          _deleted
          _lastChangedAt
          createdAt
          updatedAt
        }
        bids {
          nextToken
          startedAt
        }
        _version
        _deleted
        _lastChangedAt
        createdAt
        updatedAt
      }
      content
      dollarValue
      _version
      _deleted
      _lastChangedAt
      createdAt
      updatedAt
    }
  }
`;
export const onDeleteBid = /* GraphQL */ `
  subscription OnDeleteBid {
    onDeleteBid {
      id
      owner
      jobPostID
      jobPost {
        id
        owner
        title
        description
        image
        helpWantedID
        helpWanted {
          id
          owner
          _version
          _deleted
          _lastChangedAt
          createdAt
          updatedAt
        }
        bids {
          nextToken
          startedAt
        }
        _version
        _deleted
        _lastChangedAt
        createdAt
        updatedAt
      }
      content
      dollarValue
      _version
      _deleted
      _lastChangedAt
      createdAt
      updatedAt
    }
  }
`;
